# TRK to GPX
## Description
Python library + GUI (Tkinter) + CLI for converting TwoNav GPS tracks (format .TRK) to standard GPX, so it can be opened with many softwares (both on desktop and mobile).

## Usage
### GUI
To run the Tkinter GUI, `cd` to the folder and type `python3 Tkinter-UI.py`.

### CLI
```bash
python3 trk2gpx.py --output=output_gpx_file.gpx inputfile.trk
```
`--output` argument is optional, if omitted, the output GPX name will be derivated from the input file name (`inputfile.gpx` in the above example).

### Under the hood
TODO

## On the usage of your GPS device
You can setup your device to automatically export as GPX instead of TRK.

## Software to read GPX
TODO, both for mobile and desktop
