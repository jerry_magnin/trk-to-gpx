#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 10:43:45 2019

Base functions to convert a TwoNav .TRK file to standard .GPX.
Two interfaces are provided to these functions :
    - a Tkinter GUI
    - a simple CLI

@author: Jerry Magnin
"""

import gpxpy as gp
import gpxpy.gpx as gpp
import pandas as pd
import re

def read_trk_file(filename):
    with open(filename, 'r') as infile:
        content = infile.readlines()
    content = content[12:]
    
    sep_re = re.compile(r' {2,}')
    coords_suffix = re.compile(r'º[NSEW]')
    millis = re.compile(r'\.000 [sN]')
    
    data = []
    
    for line in content:
        line = sep_re.sub(r' ', line)
        line = coords_suffix.sub(r'', line)
        line = millis.sub(r'', line)
        data.append(line)
    
    return data

def trk_data_to_df(data):
    mat = [n.split(' ') for n in data]
    df_raw = pd.DataFrame(mat)
    df_raw = df_raw.iloc[:,(2, 3, 4, 5, 6)]
    df = df_raw
    df['TIMESTAMP'] = df.iloc[:,2] + ' ' + df.iloc[:,3]
    df['TIMESTAMP'] = pd.to_datetime(df['TIMESTAMP'])
    df = df.sort_values('TIMESTAMP')
    return df

def df_to_gpx(df, gpx_filename):
    # Go to GPX !
    gpx = gpp.GPX()
    gpx_track = gpp.GPXTrack()
    gpx.tracks.append(gpx_track)
    # Create the segment
    segment = gpp.GPXTrackSegment()
    gpx_track.segments.append(segment)
    # Adding points
    for index, line in df.iterrows():
        lon = line[1]
        lat = line[0]
        ts = line['TIMESTAMP']
        elev = line[6]
        segment.points.append(gpp.GPXTrackPoint(lat, lon, elev, ts))
    
    with open(gpx_filename, 'w') as outfile:
        outfile.write(gpx.to_xml())