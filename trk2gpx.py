#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 10:51:23 2019

@author: jerry
"""

import argparse
import dataprocessor as trk
import pathlib

parser = argparse.ArgumentParser(description='Converts TwoNav .TRK to .GPX',
                                 prog='trk2gpx')
parser.add_argument('trkfile', type=str, help='Name of the .TRK file')
parser.add_argument('--output', type=str, help='Name of the .gpx file', default=None)

args = parser.parse_args()

trk_file = args.trkfile
if args.output:
    output_gpx = args.output
else:
    output_gpx = pathlib.PurePath(trk_file).stem + '.gpx'
    
    #INPUT = '/home/jerry/Bureau/2018-12-28-01.TRK'
    #data = read_trk_file(INPUT)
    #df = trk_data_to_df(data)
    #df_to_gpx(df, '/home/jerry/Bureau/OUTPUT.gpx')
data = trk.read_trk_file(trk_file)
df = trk.trk_data_to_df(data)
trk.df_to_gpx(df, output_gpx)
print('{} converted to {}'.format(trk_file, output_gpx))